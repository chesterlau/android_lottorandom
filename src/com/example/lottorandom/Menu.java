package com.example.lottorandom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		
		Button lotto = (Button) findViewById(R.id.lotto);
		Button ozlotto = (Button) findViewById(R.id.ozlotto);
		Button powerball = (Button) findViewById(R.id.powerball);
		Button lottostrike = (Button) findViewById(R.id.lottostrike);
		Button thepools = (Button) findViewById(R.id.thepools);
		
		lotto.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.example.lottorandom.LOTTO"));
			}
		});
		
		ozlotto.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.example.lottorandom.OZLOTTO"));
			}
		});
		
		powerball.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.example.lottorandom.POWERBALL"));
			}
		});
		
		lottostrike.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.example.lottorandom.LOTTOSTRIKE"));
			}
		});
		
		thepools.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.example.lottorandom.THEPOOLS"));
			}
		});
		
	}

	
	
}
