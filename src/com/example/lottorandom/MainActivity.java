package com.example.lottorandom;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
/*import android.view.Menu;*/

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        
        Thread splashTimer = new Thread(){
        	
        	public void run(){
        		try{
        			int splashTimer = 0;
        			while(splashTimer < 3000)
        			{
        				sleep(100);
        				splashTimer+=100;
        			}
        			startActivity(new Intent("com.example.lottorandom.MENU")); 
        		}
        		
        		catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		finally{
        			finish();
        		}
        	}
        	
        };
        splashTimer.start();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }*/
}
