package com.example.lottorandom;

import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class Ozlotto extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ozlotto);
		
		Button generate = (Button) findViewById(R.id.generate);	
		
		generate.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				generate(); /* Generate random numbers */
			}
		});
		
	}
	
	public void generate()
	{
		ArrayList<Integer> al = new ArrayList<Integer>();
		Random rnd = new Random();
		EditText result1 = (EditText) findViewById(R.id.result1);
		EditText result2 = (EditText) findViewById(R.id.result2);
		EditText result3 = (EditText) findViewById(R.id.result3);
		EditText result4 = (EditText) findViewById(R.id.result4);
		EditText result5 = (EditText) findViewById(R.id.result5);
		EditText result6 = (EditText) findViewById(R.id.result6);
		EditText result7 = (EditText) findViewById(R.id.result7);
		int i=0, max = 45;
		String myResult1, myResult2, myResult3, myResult4, myResult5, myResult6, myResult7;
		
		for(i = 0; i < 7; i++)
		{
			while(true)
			{
				Integer next = rnd.nextInt(max) + 1;
		        if (!al.contains(next)) /* No repeats */
		        {
		            al.add(next); 
		            break;
		        }
			}
		}
		Collections.sort(al); /* Sort arraylist ascending order */
		
		/* Convert int to string */
		myResult1 = Integer.toString(al.get(0));
		myResult2 = Integer.toString(al.get(1));
		myResult3 = Integer.toString(al.get(2));
		myResult4 = Integer.toString(al.get(3));
		myResult5 = Integer.toString(al.get(4));
		myResult6 = Integer.toString(al.get(5));
		myResult7 = Integer.toString(al.get(6));
		
		/* Set edittext as to results */
		result1.setText(myResult1);
		result2.setText(myResult2);
		result3.setText(myResult3);
		result4.setText(myResult4);
		result5.setText(myResult5);
		result6.setText(myResult6);
		result7.setText(myResult7);
	
	}

	
}
